﻿using UnityEngine;
using System.Collections;

public class LvlSelCameraController : MonoBehaviour {

    Vector3 nextPosition;
    public float offset;
    public float lastLvlBtnLine;
    public float translateSpeed;
    public static bool isCameraInMove;

    void Start(){
        nextPosition = Vector3.zero;
        isCameraInMove = false;
    }

    void Update()
    {
        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved))
        {
            if (Input.GetTouch(0).deltaPosition.x < 0 && !isCameraInMove)
            {
                isCameraInMove = true;
                if (this.transform.position.x < lastLvlBtnLine-offset) nextPosition.x += offset;
                StartCoroutine(cameraMoveRight());
            }
            if (Input.GetTouch(0).deltaPosition.x > 0 && !isCameraInMove)
            {
                isCameraInMove = true;
                if (this.transform.position.x > offset) nextPosition.x -= offset;
                StartCoroutine(cameraMoveLeft());
            }
        }
        if (Input.GetKey(KeyCode.RightArrow) && !isCameraInMove) {
            isCameraInMove = true;
            if (this.transform.position.x < lastLvlBtnLine - offset) nextPosition.x += offset;
            StartCoroutine(cameraMoveRight());
        }
        if (Input.GetKey(KeyCode.LeftArrow) && !isCameraInMove)
        {
            isCameraInMove = true;
            if (this.transform.position.x > offset) nextPosition.x -= offset;
            StartCoroutine(cameraMoveLeft());
        }
    }

    /*
    * Первый блок корутина ведет камеру до следующего блока уровней,
    * а второй вовзращает камеру к более точному положению с меньшей скоростью
    */
    IEnumerator cameraMoveLeft()
    {
        while (this.transform.position.x > nextPosition.x)
        {
            this.transform.Translate(Vector3.left * Time.deltaTime * translateSpeed);
            yield return new WaitForFixedUpdate();
        }
        while (this.transform.position.x < nextPosition.x)
        {
            this.transform.Translate(Vector3.right * Time.deltaTime * translateSpeed/10);
            yield return new WaitForFixedUpdate();
        }
        isCameraInMove = false;
    }
    IEnumerator cameraMoveRight()
    {
        while (this.transform.position.x < nextPosition.x)
        {
            this.transform.Translate(Vector3.right * Time.deltaTime * translateSpeed);
            yield return new WaitForFixedUpdate();
        }
        while(this.transform.position.x > nextPosition.x)
        {
            this.transform.Translate(Vector3.left * Time.deltaTime * translateSpeed/10);
            yield return new WaitForFixedUpdate();
        }
        isCameraInMove = false;
    }
}
