﻿/*  этот класс - синглтон(создается в единственном экземпляре на игру)
    обращаться к нему как к статическому
    к его методам если из кода, то через статические методы(те то с большой буквы, можно через обычные, но тогда через Instance)
    если из редактора юнити, то через обычные(с маленькой, статические методы он найти не может), аргументом передается тип уровня или звука
    pitch высота звука, рандомно меняем ее звуков, чтобы были не такими одинаковыми и не надоедали
    Итог: все что с большой буквы для скриптов, с маленькой для редактора юнити
    изменять можно только префаб и код этого скрипта, объект на сцене трогать не стоит
    Везде стоит проверка Instance != null. Так как SoundManager создается в сцене menu, в остальных сценах при вызове его методов прилетает exception.
    Проверка избавляет от этого.
    Некоторые методы продублированы в статические для удобства(чтобы писать SoundManager.PlayMusic() вместо SoundManager.Instance.playMusic())
*/

using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

    public int efxAudioSourceCount = 10;
    public AudioClip clickClip;
    public float clickVolume;
    public AudioClip coinClip;
    public float coinVolume;
    public AudioClip menuMusicClip;
    public AudioClip pauseMusicClip;
    public AudioClip gameMusicClip;
    public bool pitch;
    public float lowPitchRange = 0.95f;              
    public float highPitchRange = 1.05f;
    public Sprite tuneIsOnSprite;
    public Sprite tuneIsOffSprite;
    public Sprite soundIsOnSprite;
    public Sprite soundIsOffSprite;
    private AudioSource musicSource;
    private AudioSource[] efxSource;
    private AudioSource gameMusicSource;
    bool _soundsEnabled;
    bool _musicEnabled;
    bool musicPaused;


    public static bool MusicEnabled
    {
        get
        {
            return Instance._musicEnabled;
        }
        set
        {
            Instance._musicEnabled = value;
            int n = value == true ? 1 : 0;
            PlayerPrefs.SetInt("Music Enabled", n);
            if (Instance != null)
            {
                if (value) Instance.gameMusicSource.Play();
                else Instance.gameMusicSource.Stop();
            }
            else Debug.Log("have no soundmanager");
        }
    }

    public static bool SoundsEnabled
    {
        get
        {
            return Instance._soundsEnabled;
        }
        set
        {
            if (Instance != null)
            {
                Instance._soundsEnabled = value;
            }
            int n = value == true ? 1 : 0;
            PlayerPrefs.SetInt("Sounds Enabled", n);
        }
    }

    public static SoundManager Instance { get; private set; } 

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            efxSource = new AudioSource[efxAudioSourceCount];
            for (int i = 0; i < efxAudioSourceCount; i++)
            {
                efxSource[i] = gameObject.AddComponent<AudioSource>();
            }
            musicSource = gameObject.AddComponent<AudioSource>();
            gameMusicSource = gameObject.AddComponent<AudioSource>();
            gameMusicSource.loop = true;
            musicSource.loop = true;
            _musicEnabled = PlayerPrefs.GetInt("Music Enabled", 1) == 0 ? false : true;
            _soundsEnabled = PlayerPrefs.GetInt("Sounds Enabled", 1) == 0 ? false : true;
            musicPaused = false;
            //PlayMenuMusic();
            PlayGameMusic();
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        setTexture("tuneBtn", MusicEnabled ? tuneIsOnSprite : tuneIsOffSprite);
        setTexture("soundBtn", SoundsEnabled ? soundIsOnSprite : soundIsOffSprite);

    }

    public void playMusic(string typeOfLevel)
    {
        switch (typeOfLevel)
        {
            case "menu":
                Instance.musicSource.clip = menuMusicClip;
                if (MusicEnabled) Instance.musicSource.Play();
                break;
            case "game":
                Instance.musicSource.Stop();
                Instance.gameMusicSource.clip = gameMusicClip;
                if (MusicEnabled) Instance.gameMusicSource.Play();
                break;
            case "pause":
                Instance.musicSource.clip = pauseMusicClip;
                if (MusicEnabled) Instance.musicSource.Play();
                break;
            default:
                Debug.Log("SoundMeneger: have no such type of level");
                break;
        }
    }

    public void playSound(string typeOfSound)
    {
        if (SoundsEnabled && !LvlSelCameraController.isCameraInMove)
        {
            AudioSource freeSource = Instance.findFreeEfxSource();//возвращает null если все источники заняты
            if (freeSource != null)
            {
                switch (typeOfSound)
                {
                    case "click":
                        freeSource.clip = clickClip;
                        freeSource.volume = clickVolume;
                        break;
                    case "coin":
                        freeSource.clip = coinClip;
                        freeSource.volume = coinVolume;
                        break;
                    default:
                        Debug.Log("SoundMeneger: have no such type of sound");
                        break;
                }
                if (pitch) freeSource.pitch = Random.Range(lowPitchRange, highPitchRange);
                freeSource.loop = false;
                freeSource.Play();
            }
            else
            {
                Debug.Log("not enough efxAudioSources");
            }
        }
    }

    private AudioSource findFreeEfxSource()
    {
        foreach(AudioSource source in Instance.efxSource)
        {
            if (!source.isPlaying) return source;
        }
        return null;
    }//возвращает null если все источники заняты

    public void switchMusicEnabled()//для работы из нового модного интерфейса
    {
        MusicEnabled = !MusicEnabled;
        setTexture("tuneBtn", MusicEnabled ? tuneIsOnSprite : tuneIsOffSprite);
    }

    public void switchSoundsEnabled() //для работы из нового модного интерфейса
    {
        SoundsEnabled = !SoundsEnabled;
        setTexture("soundBtn", SoundsEnabled ? soundIsOnSprite : soundIsOffSprite);
    }

    private void setTexture(string buttonName, Sprite spriteToSet)// меняет спрайт на кнопке(вкл/выкл музыки и звуков)
    {
        Image texture = GameObject.Find(buttonName).GetComponent<Image>();
        texture.sprite = spriteToSet;
    }

    public static void PlayMenuMusic()
    {
        if (Instance != null) Instance.playMusic("menu");
        else Debug.Log("have no soundmanager");
    }

    public static void PlayGameMusic()
    {
        if (Instance != null) Instance.playMusic("game");
        else Debug.Log("have no soundmanager");
    } 

    public static void PlayPauseMusic()
    {
        if (Instance != null) Instance.playMusic("pause");
        else Debug.Log("have no soundmanager");
    }

    public static void PauseTheMusic()
    {
        if (Instance != null) Instance.pauseTheMusic();
        else Debug.Log("have no soundmanager");
    }

    public void pauseTheMusic()
    {
        if (MusicEnabled)
        {
            gameMusicSource.Pause();
            PlayPauseMusic();
            musicPaused = true;
        }
    }

    public static void UnPauseTheMusic()
    {
        if (Instance != null) Instance.unPauseTheMusic();
        else Debug.Log("have no soundmanager");
    }

    public void unPauseTheMusic()
    {
        if (MusicEnabled)
        {
            if (musicPaused)
            {
                musicSource.Stop();
                gameMusicSource.UnPause();
                musicPaused = false;
            }
        }
    }

    public static void PlayClick()
    {
        if (Instance != null) Instance.playSound("click");
        else Debug.Log("have no soundmanager");
    }

    public static void PlayCoinSound()
    {
        if (Instance != null) Instance.playSound("coin");
        else Debug.Log("have no soundmanager");
    }
}
