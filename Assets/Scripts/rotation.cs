﻿using UnityEngine;
using System.Collections;

public class rotation : MonoBehaviour {
	private int clockwise=1;
	public float rotSpeed;
	void Update () {
        if (rotMove.isControlEnable)
        {
            if (Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.X))
            {//при нажатии альта,X или энтера на нумлоке меняем сторону, в которую вращаемся
                clockwise *= -1;
            }
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && Mathf.Abs(Input.GetTouch(0).deltaPosition.x) > 10)//меняем сторону вращения в зависимости от свайпа
            {
                if (Input.GetTouch(0).deltaPosition.x > 0) { clockwise = -1; }
                if (Input.GetTouch(0).deltaPosition.x < 0) { clockwise = 1; }
            }
        }
            this.transform.Rotate (new Vector3 (0, 0, 1*clockwise) * Time.deltaTime * rotSpeed);//вращаем ротор по Z

	}
}
