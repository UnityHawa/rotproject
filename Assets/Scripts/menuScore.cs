﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class menuScore : MonoBehaviour {

    private int currentScore;
    public Text cScoreText;

    void Start () {
        setScore();
        currentScore = 0;
    }
    void FixedUpdate()//fixed таймскейлом тормозится, а простой - нет
    {
            if (currentScore != cookieManager.cScore)
            {
                currentScore = cookieManager.cScore;
                setScore();
            }


    }

    void setScore(){
        cScoreText.text = currentScore.ToString();
    }
}
