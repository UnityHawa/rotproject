﻿using UnityEngine;

[ExecuteInEditMode]

public class TriangleColliderCreator : MonoBehaviour
{

    void Start()
    {
        foreach (Transform child in transform)
        {
            EdgeCollider2D coll = child.gameObject.AddComponent<EdgeCollider2D>();
            coll.isTrigger = true;
            coll.points = new Vector2[] { new Vector2(4.23f, 3.66f),
                                      new Vector2(-4.23f, 3.65f),
                                      new Vector2(-0.015f, -3.67f),
                                      new Vector2(4.23f, 3.66f) };
        }

    }
}