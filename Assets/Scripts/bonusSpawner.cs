﻿/*Как использовать префаб bonusSpawner:
    - в инспекторе определить начало и конец диапазона
    - добавить бонусы в префаб как в контейнер
    - сделать их неактивными
    - установить вероятность выпадения бонусов
    - успех
*/

using UnityEngine;
using System.Collections;

public class bonusSpawner : MonoBehaviour
{

    Transform[] bonuses;
    public Transform rotor;
    public Transform lastBlock;
    public float minY;
    public float maxY;
    public float chance; //вероятность выпадения бонусов, принимает значения от 0 до 1
    Vector3 freePosition;

    void Start()
    {
        StartCoroutine(spawn());
    }

    /**
    *Этот корутин генерирует бонусы, делая паузу после каждой неудачной попытки
    *поиска пустого места, а также ставит паузу после каждого созданного бонуса
    */
    IEnumerator spawn()
    {
        foreach (Transform childBonus in transform)
        {
            if (Random.Range(0f, 1f) <= chance)//если случайное число < шанса выпадения, выкидываем бонус
            {
                bool isPositionFree = false;
                while (!isPositionFree)
                {
                    freePosition = new Vector3(Random.Range(rotor.position.x + 10, lastBlock.position.x - 20), Random.Range(minY, maxY), 0);
                    //Debug.Log("rotor position is " + rotor.position.x);
                    if (!Physics2D.OverlapCircle(freePosition, 2f))// смотрим, есть ли что-то в окружности  c центром в freePosition
                    {
                        isPositionFree = true;
                       // Debug.Log("free position is " + freePosition);
                    }
                    //else
                    //{
                        //Debug.Log("position " + freePosition + " is not empty");
                    //}
                    yield return new WaitForSeconds(0.1f);
                }
                childBonus.position = freePosition;
                childBonus.gameObject.SetActive(true);
            }
            yield return new WaitForFixedUpdate();
        }

    }
}