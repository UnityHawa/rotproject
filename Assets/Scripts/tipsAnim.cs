﻿using UnityEngine;
using UnityEngine.UI;


public class tipsAnim : MonoBehaviour {
    public GameObject continueButton;
    float rotorRotSpeed;//скорость вращения
    float rotorSpeed;//скорость передвижения ротора
    float circ1RadiusDown;//скорость уменьшения радиуса серка
    float circ2RadiusDown;
    int i;
    bool isTipTime;
    Animator tips;

	void Start () {
        tips = GetComponent<Animator>();
        i = 1;
        isTipTime = false;
    }

    void Update()
    {
        if (isTipTime)
        {
            if (Input.GetKey(KeyCode.Space) || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary)
            { //возвращаем параметры ротора и продолжаем игру
                returnRotorParam();
                isTipTime = false;
            }
        }
    }

    void getRotorParam()
    {
        rotorRotSpeed = GameObject.Find("rotor").GetComponent<rotation>().rotSpeed;
        rotorSpeed = GameObject.Find("rotor").GetComponent<rotMove>().currentSpeed;
        circ1RadiusDown = GameObject.Find("circ 1").GetComponent<radiusChange>().downSpeed;
        circ2RadiusDown = GameObject.Find("circ 2").GetComponent<radiusChange>().downSpeed;
    }
    void returnRotorParam()
    {
        GameObject.Find("rotor").GetComponent<rotation>().rotSpeed = rotorRotSpeed;
        GameObject.Find("rotor").GetComponent<rotMove>().currentSpeed = rotorSpeed;
        GameObject.Find("circ 1").GetComponent<radiusChange>().downSpeed = circ1RadiusDown;
        GameObject.Find("circ 2").GetComponent<radiusChange>().downSpeed = circ2RadiusDown;
    }
    void stopRotor()
    {
        GameObject.Find("rotor").GetComponent<rotation>().rotSpeed = 0;
        GameObject.Find("rotor").GetComponent<rotMove>().currentSpeed = 0;
        GameObject.Find("circ 1").GetComponent<radiusChange>().downSpeed = 0;
        GameObject.Find("circ 2").GetComponent<radiusChange>().downSpeed = 0;
    }

    //public void continueGame()//возвращаем параметры ротора и продолжаем игру
    //{
    //    returnRotorParam();
    //    isTipTime = false;
    //    continueButton.SetActive(false);
    //}

    public void showTip()
    {
        isTipTime = true;
        getRotorParam();
        stopRotor();
       // continueButton.SetActive(true);
        tips.SetBool("tip" + i + "Start", true);
        i++;
    }

}
