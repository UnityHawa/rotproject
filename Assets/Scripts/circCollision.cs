﻿using UnityEngine;
using System.Collections;

public class circCollision : MonoBehaviour {
    public GameObject shield;

    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "block": //если нет щита, серк умирает, иначе запускаем корутин уничтожения препятствия 
                if (!shield.activeInHierarchy)
                {
                    StartCoroutine(destroyCirc());
                    //this.gameObject.SetActive(false);
                }
                else
                {
                    StartCoroutine(destroyBlock(other));
                }
                break;

            case "DEATH":
                this.gameObject.SetActive(false);
                break;

            case "shieldBonus":
                if (!shield.activeInHierarchy)
                {
                    shield.SetActive(true);
                    other.gameObject.SetActive(false);
                }
                else//если на серке уже есть щит, а он поднимает еще один, новый надо отключить
                {
                    other.gameObject.SetActive(false);
                }
                break;
            case "tipStarter":
                GameObject.Find("tips").GetComponent<tipsAnim>().showTip();
                other.gameObject.SetActive(false);
                break;
        }
    }

    IEnumerator destroyBlock(Collider2D block)
    {
        while (block.transform.localScale.x > 0 && block.transform.localScale.y > 0)
        {
            block.isTrigger = false; //как обработали столкновение, нам больше не надо за ним следить, иначе фрапс проседает, когда их много
            block.transform.localScale -= (new Vector3(0.1f, 0.1f, 0));
            block.transform.rotation = Random.rotation;
            yield return new WaitForSeconds(0.03f);
        }
        //выключаем щит и препятствие
        block.gameObject.SetActive(false);
        shield.SetActive(false);
    }

    IEnumerator destroyCirc()
    {
        while (transform.localScale.x > 0.05 && transform.localScale.y > 0.05)
        {
            this.gameObject.GetComponent<Collider2D>().isTrigger = false; //как обработали столкновение, нам больше не надо за ним следить, иначе фрапс проседает, когда их много
            transform.localScale -= (new Vector3(0.05f, 0.05f, 0));
            yield return new WaitForFixedUpdate();
        }
        //выключаем circ
        this.gameObject.SetActive(false);
    }

}
