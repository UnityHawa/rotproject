﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class circLineRend : MonoBehaviour {

    LineRenderer lineRen;//рисовалка
    int vertexCount;//номер вершины
    public GameObject circ;
    public int pointsToDraw;//сколько точек собрать прежде чем интерполировать(минимум 3, иначе будет просто прямая)
    public int pointsPerSegment;
    public bool isChordal = false;//метод интерполяции
    List<Vector3> vecs;//сохраняет последние векторы в количестве pointsToDraw
    List<Vector3> interpolatedVecs;//хранит все интерполлированные векторы


	void Start () {
        lineRen = GetComponent<LineRenderer>();
        vertexCount = 0;
        vecs = new List<Vector3>(pointsPerSegment);
        interpolatedVecs = new List<Vector3>();
        StartCoroutine(linRenCoroutine());
    }

    /*IEnumerator linRenCoroutine()
    {
        while (circ.activeInHierarchy) {
            lineRen.SetVertexCount(vertexCount--);
            lineRen.SetPosition(vertexCount, circ.transform.position);
            vertexCount += 2;
            yield return new WaitForSeconds(0.033333334f);
        }
    }*/

    IEnumerator linRenCoroutine()
    {
        while (circ.activeInHierarchy)
        {
            vecs.Add(circ.transform.position);
            if (vecs.Count >= pointsToDraw)
            {
                interpolatedVecs.AddRange(CatmullRomCurve.interpolate(vecs, pointsPerSegment, isChordal));
                setLinePositions(interpolatedVecs.ToArray());
                vecs.RemoveRange(0, pointsToDraw - 2);
            }
            else//пока собираем векторы для интерполяции рисуем по обычным точкам, когда насобираем перерисуем
            {
                addLinePosition(circ.transform.position);
            }
            yield return new WaitForSeconds(0.033333334f);
        }
    }

    void addLinePosition(Vector3 position)
    {
        vertexCount++;
        lineRen.SetVertexCount(vertexCount);
        lineRen.SetPosition(vertexCount - 1, position);
    }

    void addLinePositions(Vector3[] positions)
    {
        foreach (Vector3 vec in positions) addLinePosition(vec);
    }

    void setLinePositions(Vector3[] positions)
    {
        vertexCount = interpolatedVecs.Count;
        lineRen.SetVertexCount(interpolatedVecs.Count);
        lineRen.SetPositions(interpolatedVecs.ToArray());
    }

}