﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class reset : MonoBehaviour {
	public GameObject circ1;
	public GameObject circ2;
	public float speed;
    bool isAlive = true;
    private int totalScore;

    void Start()
    {
        PlayerPrefs.SetString("currentLevel", Application.loadedLevelName);
    }

	void OnTriggerEnter2D(Collider2D other){//если ротор встречается с DEAТH, то перезапускаем уровень
			if (other.gameObject.CompareTag ("DEATH")) {
            circ1.SetActive(false);
            circ2.SetActive(false);
            isAlive = false;
            reloadLevel();

        }

	}
	void Update(){//если хоть один из шаров отключен, отправляем второй в увлекательное путешествие
        if (isAlive) {//если ротор жив, перезапускаем уровень и говорим ему, что он мертв, иначе он бесконечно ломится в этот апдейт
            if (!circ1.activeInHierarchy || !circ2.activeInHierarchy) {
                circ1.transform.Translate(Vector3.right * Time.deltaTime * speed);
                circ2.transform.Translate(Vector3.right * Time.deltaTime * speed * -1.0f);
            }//а если оба отключены, то просто перезапускаем уровень
            if (!circ1.activeInHierarchy && !circ2.activeInHierarchy) {
                isAlive = false;
                reloadLevel();
            }
        }
	}

    void reloadLevel()
    {
        Timer.disableAllTimers();
        GameObject.Find("scoreCanvas").GetComponent<loadSceneOnClick>().Pause(isAlive);
        setTotalScore();
        //Debug.Log("totalScore: " + totalScore);
    }

    void setTotalScore()
    {
        totalScore = PlayerPrefs.GetInt("totalScore");
        if (totalScore != cookieManager.cScore)
        {
            totalScore += cookieManager.cScore;
            PlayerPrefs.SetInt("totalScore", totalScore);
        }
    }

}

