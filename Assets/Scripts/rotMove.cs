﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class rotMove : MonoBehaviour {
	Rigidbody2D rb;
    public float currentSpeed;
    private Vector3 startPosition;
    public float downSpeed;
    private bool isAlive;
    public GameObject circ1;
    public GameObject circ2;
    public static bool isControlEnable;
    public float controlEnableBorder;

    void Start(){
		rb=GetComponent<Rigidbody2D> ();
        startPosition = this.transform.position;
        isAlive = true;
        isControlEnable = false;
    }

	// двигаем ротор вдоль оси X со скоростью speed
	void Update () {
        if (!isControlEnable && this.transform.position.x > controlEnableBorder) isControlEnable = true;
        if (isAlive)
        {
            rb.velocity = new Vector2(currentSpeed, 0);
        }
        else //если ротор помер, откатываем его вместе с камерой на старт уровня
        {
            rb.velocity = new Vector2(-downSpeed, 0);
            if (this.transform.position.x <= startPosition.x)
            {
                rb.velocity = new Vector2(0, 0);
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {//если встречаемся с DEAТH, то откатываем ротор на старт
        if (other.gameObject.CompareTag("DEATH") || (!circ1.activeInHierarchy || !circ2.activeInHierarchy))
        {
            Time.timeScale = 1;
            isAlive = false;
        }
    }
}
