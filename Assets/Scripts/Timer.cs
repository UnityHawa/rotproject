﻿using UnityEngine;

public class Timer : MonoBehaviour {

    bool isOn = false;
    float timerTime;
    System.Action action;//действие которое выполнится по окончании таймера
	
	void Update () {
        if (isOn)
        {
            if (timerTime > 0) { timerTime -= Time.deltaTime; }
            if (timerTime <= 0)
            {
                action();
                disable();
            }
        }
	}

    public void run(float timeOfAction, System.Action newAction)//принимает делегат(функцию)
    {
        if (timeOfAction < 0) return;
        isOn = true;
        timerTime = timeOfAction;
        action = newAction;        
    }

    public void disable()
    {
        isOn = false;
        this.gameObject.SetActive(false);
    }

    public static void disableAllTimers()
    {
        GameObject[] allTimers = GameObject.FindGameObjectsWithTag("TIMER");
        if (allTimers != null)
        {
            for (int i = 0; i < allTimers.Length; i++)
            {
                allTimers[i].GetComponent<Timer>().disable();
            }
        }
    }
}
