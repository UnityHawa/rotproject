﻿using UnityEngine;

public class cameraController : MonoBehaviour {

	private Vector3 offset;
	GameObject player;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
		offset = this.transform.position - player.transform.position;
	}
	
	void LateUpdate () {
        this.transform.position = player.transform.position + offset;
	}
}
