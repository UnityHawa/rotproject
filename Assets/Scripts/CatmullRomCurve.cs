﻿/* Этот класс переделан из java класса найденного в интернете.
 * поэтому комменты на английском. Мне лень переводить:))
 */

using UnityEngine;
using System.Collections.Generic;

public static class CatmullRomCurve
{
    public static List<Vector3> interpolate(List<Vector3> coordinates,int pointsPerSegment, bool chordalCurve = false)
    {
        List<Vector2> coord2d = new List<Vector2>(coordinates.Count);
        foreach (Vector2 vec in coordinates) coord2d.Add(vec);
        List<Vector2> vecs2d =  interpolate(coord2d, pointsPerSegment, chordalCurve);
        List<Vector3> vecs3d = new List<Vector3>();
        foreach (Vector2 vec in vecs2d) vecs3d.Add(vec);
        return vecs3d;
    }
    /* @param pointsPerSegment The integer number of equally spaced points to
     * return along each curve.  The actual distance between each
     * point will depend on the spacing between the control points.
     * @return The list of interpolated coordinates.
     * @param curveType Chordal (stiff), Uniform(floppy), or Centripetal(medium)
     * @throws gov.ca.water.shapelite.analysis.CatmullRomException if
     * pointsPerSegment is less than 2.
     */
    public static List<Vector2> interpolate(List<Vector2> coordinates, int pointsPerSegment, bool chordalCurve = false)
    {
        List<Vector2> vertices = new List<Vector2>();
        foreach (Vector2 vec in coordinates)
        {
            vertices.Add(vec);
        }
        if (pointsPerSegment< 2) {
            Debug.Log("СatmullRomCurve: The pointsPerSegment parameter must be greater than 2");
            return coordinates;
        }

        if (vertices.Count < 3)
        {
            Debug.Log("СatmullRomCurve: need more than 2 points to interpolate");
            return vertices;
        }

            //use control points that simply extend
            // the first and last segments

            // Get the change in x and y between the first and second coordinates.
            float dx = vertices[1].x - vertices[0].x;
            float dy = vertices[1].y - vertices[0].y;

            // Then using the change, extrapolate backwards to find a control point.
            float x1 = vertices[0].x - dx;
            float y1 = vertices[0].y - dy;

            // Actaully create the start point from the extrapolated values.
            Vector2 start = new Vector2(x1, y1);

            // Repeat for the end control point.
            int n = vertices.Count - 1;
            dx = vertices[n].x - vertices[n - 1].x;
            dy = vertices[n].y - vertices[n - 1].y;
            float xn = vertices[n].x + dx;
            float yn = vertices[n].y + dy;
            Vector2 end = new Vector2(xn, yn);

            // insert the start control point at the start of the vertices list.
            vertices.Insert(0, start);

            // append the end control ponit to the end of the vertices list.
            vertices.Add(end);
        //}

        // Dimension a result list of coordinates. 
        List<Vector2> result = new List<Vector2>();
        // When looping, remember that each cycle requires 4 points, starting
        // with i and ending with i+3.  So we don't loop through all the points.
        for (int i = 0; i < vertices.Count - 3; i++)
        {

            // Actually calculate the Catmull-Rom curve for one segment.
            List<Vector2> points = interpolate(vertices, i, pointsPerSegment, chordalCurve);
            // Since the middle points are added twice, once for each bordering
            // segment, we only add the 0 index result point for the first
            // segment.  Otherwise we will have duplicate points.
            if (result.Count > 0)
            {
                points.RemoveAt(0);
            }

            // Add the coordinates for the segment to the result list.
            result.AddRange(points);
        }
        return result;

    }

    /**
     * Given a list of control points, this will create a list of pointsPerSegment
     * points spaced uniformly along the resulting Catmull-Rom curve.
     *
     * @param points The list of control points, leading and ending with a 
     * coordinate that is only used for controling the spline and is not visualized.
     * @param index The index of control point p0, where p0, p1, p2, and p3 are
     * used in order to create a curve between p1 and p2.
     * @param pointsPerSegment The total number of uniformly spaced interpolated
     * points to calculate for each segment. The larger this number, the
     * smoother the resulting curve.
     * @param curveType Clarifies whether the curve should use uniform, chordal
     * or centripetal curve types. Uniform can produce loops, chordal can
     * produce large distortions from the original lines, and centripetal is an
     * optimal balance without spaces.
     * @return the list of coordinates that define the CatmullRom curve
     * between the points defined by index+1 and index+2.
     */
    public static List<Vector2> interpolate(List<Vector2> points, int index, int pointsPerSegment, bool chordalCurve)
    {
        List<Vector2> result = new List<Vector2>();
        float[] x = new float[4];
        float[] y = new float[4];
        float[] time = new float[4];
        for (int i = 0; i < 4; i++)
        {
            x[i] = points[index + i].x;
            y[i] = points[index + i].y;
            time[i] = i;
        }

        float tstart = 1;
        float tend = 2;
        if (chordalCurve == true) { 
        //if (!curveType.equals(CatmullRomType.Uniform)) {
            float total = 0;
            for (int i = 1; i < 4; i++) {
                float dx = x[i] - x[i - 1];
                float dy = y[i] - y[i - 1];
                /*if (curveType.equals(CatmullRomType.Centripetal)) {
                    total += Math.pow(dx * dx + dy * dy, .25);
                } else {*/
                    total += Mathf.Pow(dx * dx + dy * dy, 0.5f);
                //}
                time[i] = total;
            }
            tstart = time[1];
            tend = time[2];
        }
        int segments = pointsPerSegment - 1;
        result.Add(points[index + 1]);
        for (int i = 1; i < segments; i++)
        {
            float xi = interpolate(x, time, tstart + (i * (tend - tstart)) / segments);
            float yi = interpolate(y, time, tstart + (i * (tend - tstart)) / segments);
            result.Add(new Vector2(xi, yi));
        }
        result.Add(points[index + 2]);
        return result;
    }

    /**
     * Unlike the other implementation here, which uses the default "uniform"
     * treatment of t, this computation is used to calculate the same values but
     * introduces the ability to "parameterize" the t values used in the
     * calculation. This is based on Figure 3 from
     * http://www.cemyuksel.com/research/catmullrom_param/catmullrom.pdf
     *
     * @param p An array of double values of length 4, where interpolation
     * occurs from p1 to p2.
     * @param time An array of time measures of length 4, corresponding to each
     * p value.
     * @param t the actual interpolation ratio from 0 to 1 representing the
     * position between p1 and p2 to interpolate the value.
     * @return
     */
    public static float interpolate(float[] p, float[] time, float t)
    {
        float L01 = p[0] * (time[1] - t) / (time[1] - time[0]) + p[1] * (t - time[0]) / (time[1] - time[0]);
        float L12 = p[1] * (time[2] - t) / (time[2] - time[1]) + p[2] * (t - time[1]) / (time[2] - time[1]);
        float L23 = p[2] * (time[3] - t) / (time[3] - time[2]) + p[3] * (t - time[2]) / (time[3] - time[2]);
        float L012 = L01 * (time[2] - t) / (time[2] - time[0]) + L12 * (t - time[0]) / (time[2] - time[0]);
        float L123 = L12 * (time[3] - t) / (time[3] - time[1]) + L23 * (t - time[1]) / (time[3] - time[1]);
        float C12 = L012 * (time[2] - t) / (time[2] - time[1]) + L123 * (t - time[1]) / (time[2] - time[1]);
        return C12;
    }
}