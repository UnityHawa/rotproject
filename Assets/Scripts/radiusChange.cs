﻿using UnityEngine;
using System.Collections;


public class radiusChange : MonoBehaviour {
	public float speed;
	public float downSpeed;
    public float maxRadius;

	void Update () {
		if (rotMove.isControlEnable && (Input.GetKey (KeyCode.Space) || Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved && Mathf.Abs(Input.GetTouch(0).deltaPosition.x) <= 10))) {//при нажатии пробела/длинного тапа увеличиваем радиус орбиты шаров
			if (Mathf.Abs (this.transform.localPosition.x) < maxRadius) {
				this.transform.Translate (Vector3.right * Time.deltaTime * speed);
			}
		} else {//автоматически возвращаем радиус орбиты на исходное значение
			if (Mathf.Abs (this.transform.localPosition.x) > 1.0) {
				this.transform.Translate (Vector3.left * Time.deltaTime * downSpeed);
			}
		}
	}
}
