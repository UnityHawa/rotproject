﻿using UnityEngine;
using System.Collections;

public class logoTrAnim : MonoBehaviour {
    public float minRotationSpeed;
    public float maxRotationSpeed;
    public float maxX;
    public float minX;
    public float maxY;
    public float minY;
    private bool isTrActive;
    public bool isRandPosEnable;
    private float rotationSpeed;
    private int clockwise;

	void Start ()
    {
        isTrActive = true;
        /*рандомно ставим размер, активность, скорость и направление вращения*/
        if (isRandPosEnable)
        {
            if (Random.Range(0f, 1f) >= 0.5f)
            {
                this.gameObject.SetActive(false);
                isTrActive = false;
            }
            if (isTrActive)
            {
                float size = Random.Range(0.5f, 1f);
                this.gameObject.transform.localScale = new Vector3(size, size, 0);
            }

            //while (!isPositionFree)
            //{
            //    freePosition = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0);
            //    if (!Physics2D.OverlapCircle(freePosition, 1f, 8))// смотрим, есть ли что-то в окружности  c центром в freePosition
            //    {
            //        isPositionFree = true;
            //    }
            //    else
            //    {
            //        Debug.Log("pos is not empty: " + freePosition);
            //    }

                //}
                //transform.localPosition = freePosition;
        }

        rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);
        clockwise=(Random.value > 0.5 ? 1 : -1);
    }
	
	void Update () {
        this.transform.Rotate(new Vector3(0, 0, 1 * clockwise) * Time.deltaTime * rotationSpeed);//вертим треугольники
    }
}