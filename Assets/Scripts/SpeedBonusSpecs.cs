﻿using UnityEngine;

public class SpeedBonusSpecs : MonoBehaviour {

    public float timeScale;//во сколько раз замедлить время
    public float timeOfAction;//время действия бонуса
    public GameObject timerObject;//префаб таймера, можно и объект поставить, но использоваться будет его клон
    private static GameObject speedBonusTimer;//объект таймер (1 таймер на все бонусы ускорения, поэтому static)

    void Start()
    {
        if (timeScale <= 0 || timeOfAction <= 0)//если заданы плохие параметры пишем в лог и отключаем бонус
        {
            Debug.Log("SpeedBonus: bad parameters");
            this.gameObject.SetActive(false);
        }
        if (speedBonusTimer == null)//создаем объект таймера, если его еще не создал другой бонус
        {
            speedBonusTimer = Instantiate(timerObject);
            speedBonusTimer.gameObject.SetActive(false);//отключаем его т.к. пока он нам не нужен
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "circ")
        {
            Time.timeScale =  timeScale;//замедляем или ускоряем время
            speedBonusTimer.gameObject.SetActive(true);
            speedBonusTimer.GetComponent<Timer>().run(timeOfAction, delegate () { Time.timeScale = 1; });//запускаем таймер и передаем ему данные
            this.gameObject.SetActive(false);
        }
    }
}
