﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class loadSceneOnClick : MonoBehaviour {

    public GameObject pauseButton;
    public GUIStyle totalLabel;
    public GUIStyle totalLabelScore;
    public GUIStyle resumeBtnStyle;
    public GUIStyle restartBtnStyle;
    public GUIStyle menuBtnStyle;
    public Texture2D bgPauseTexture;//прозрачный фон для меню паузы
    private bool pauseMode = false;
    private bool isAlive;
    public Vector2 btnPosition;//расположение элементов на экране
    public Vector2 labelPosition;
    public float offset;//сдвиг кнопок
    public Vector2 buttonSize;
    public float distance;//расстояние между кнопками
    public Vector2 maxScreenSize;
    private Vector2 scale;// коэффициент изменения размеров элементов

   
	public void StartLevel (string levelName)
    {
        if (!LvlSelCameraController.isCameraInMove)
        {
            cookieManager.cScore = 0;
            SceneManager.LoadScene(levelName);
        }
    }
	
    public void RestartLevel()
    {
        cookieManager.cScore = 0;
        string currentLevel = PlayerPrefs.GetString("currentLevel");
        SceneManager.LoadScene(currentLevel);
        //SoundManager.PlayGameMusic();
    }

    public void Pause(bool alive)
    {
        pauseButton.SetActive(false);
        if (alive) {
            Time.timeScale = 0;
            SoundManager.PauseTheMusic();
        }
        pauseMode = true;
        isAlive = alive;
        
    }

    private void OnGUI()// тут менюшка создается скриптовая
    {
        if (pauseMode)
        {  

            GUI.DrawTexture(new Rect(0,0, maxScreenSize.x, maxScreenSize.y), bgPauseTexture,ScaleMode.StretchToFill);
            screenResize();
            if (isAlive) {//если скрипт вызван из-за смерти ротора, то кнопки продолжить быть не должно
                if (GUI.Button(new Rect(btnPosition.x, btnPosition.y, buttonSize.x, buttonSize.y), "", resumeBtnStyle))
                {
                    pauseButton.SetActive(true);
                    Time.timeScale = 1;
                    pauseMode = false;
                    SoundManager.PlayClick();
                    SoundManager.UnPauseTheMusic();
                }
            }



            if (GUI.Button(new Rect(btnPosition.x+offset, btnPosition.y + distance, buttonSize.x, buttonSize.y), "", restartBtnStyle))
            {
                RestartLevel();
                Time.timeScale = 1;
                pauseMode = false;
                SoundManager.PlayClick();
                SoundManager.UnPauseTheMusic();
            }

            if (GUI.Button(new Rect(btnPosition.x+offset * 2, btnPosition.y + distance * 2, buttonSize.x , buttonSize.y), "", menuBtnStyle))
            {
                SoundManager.PlayClick();
                StartLevel("menu");
                Time.timeScale = 1;
                pauseMode = false;
                //SoundManager.PlayMenuMusic();
                SoundManager.UnPauseTheMusic();
            }

            GUI.Label(new Rect(labelPosition.x, labelPosition.y, 712, 147), "Total: ", totalLabel);
            GUI.Label(new Rect(labelPosition.x + 400, labelPosition.y + 5, 712, 147), PlayerPrefs.GetInt("totalScore").ToString(), totalLabelScore);
            GUI.matrix = Matrix4x4.identity;
        }

    }

    private void screenResize()//подгоняем кнопки под размер экрана
    {
        scale = new Vector2(Screen.width / maxScreenSize.x, Screen.height / maxScreenSize.y);
        Matrix4x4 resizeMtx = Matrix4x4.identity;
        resizeMtx.SetTRS(new Vector3(1, 1, 1), Quaternion.identity, new Vector3(scale.x, scale.y, 1));
        GUI.matrix = resizeMtx;
    }

}
